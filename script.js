let initialData = () => ({
    molarity: 0,
    pieces: 1,
    temp: 273,
    gas: 0,
    flask: {
        size: 150,
        X: 200,
        Y: 500
    }
});

let app = new Vue({
    el: "#app",
    data: () => initialData(),
    computed: {
        // 2HCl + Zn => H2 + ZnCl
        rate: function() {
            // default rate at 1 molarity, 1 piece, and room temperature (293 K)
            // in bubbles per tick
            // doubles every 10 degrees
            return (this.molarity * this.pieces * ((this.temp/293)/10)) * 10;
        }
    },
    methods: {
        restart: function() {
            Object.assign(this.$data, initialData());
            this.drawFlask();
        },
        updateGas: function() {
            if (this.gas < 3000) {
                this.gas += this.rate;
            }
        },
        drawFlask: function() {
          
            // clear the previous frame before rendering the next
            $('#beaker').clearCanvas()

            // the hot plate !

            // the hot plate neck
            $('#beaker').drawRect({
                fillStyle: '#24261f',
                x: this.flask.X, y: this.flask.Y+this.flask.size*.14,
                width: this.flask.size*.8,
                height: this.flask.size*.05,
                cornerRadius: this.flask.size*.1
            });

            // the hot plate bottom
            $('#beaker').drawRect({
                fillStyle: '#5a6340',
                x: this.flask.X, y: this.flask.Y+this.flask.size*.43,
                width: this.flask.size*1.15,
                height: this.flask.size*.55,
                cornerRadius: this.flask.size*.1
            });

            // the hot plate top
            $('#beaker').drawRect({
                fillStyle: '#7e856e',
                x: this.flask.X, y: this.flask.Y+this.flask.size*.027,
                width: this.flask.size*1.1,
                height: this.flask.size*.2,
                cornerRadius: this.flask.size*.1
            });

            // the hot plate knob
            $('#beaker').drawArc({
                fillStyle: '#535749',
                x: this.flask.X+this.flask.size*.3, y: this.flask.Y+this.flask.size*.55,
                radius: this.flask.size*.1
            });

            // the hot plate knob thing to move it
            $('#beaker').drawArc({
                fillStyle: 'black',
                x: this.flask.X+this.flask.size*.33, y: this.flask.Y+this.flask.size*.57,
                radius: this.flask.size*.03
            });

            // temperature border
            $('#beaker').drawRect({
                fillStyle: 'black',
                x: this.flask.X, y: this.flask.Y+this.flask.size*.35,
                width: this.flask.size*.4,
                height: this.flask.size*.25,
                cornerRadius: this.flask.size*.03
            });

            // the temperature
            $('#beaker').drawText({
                fillStyle: '#9cf',
                strokeStyle: '#25a',
                strokeWidth: 2,
                x: this.flask.X, y: this.flask.Y+this.flask.size*.35,
                fontSize: this.flask.size*.1,
                fontFamily: 'Verdana, sans-serif',
                text: (this.temp - 273) + '°C'
            });

            /// the flask !

            // base of the beaker
            $('#beaker').drawEllipse({
                strokeStyle: 'black',
                fillStyle: 'rgba(179, 224, 255, 0.9)',
                x: this.flask.X, y: this.flask.Y,
                width: this.flask.size, height: this.flask.size*.1
            });
          
            // left side of the beaker
            $('#beaker').drawLine({
                strokeStyle: 'black',
                x1: this.flask.X-this.flask.size/2, y1: this.flask.Y,
                x2: this.flask.X-this.flask.size*.15, y2: this.flask.Y-this.flask.size
            })

            // right side of the beaker
            $('#beaker').drawLine({
                strokeStyle: 'black',
                x1: this.flask.X+this.flask.size/2, y1: this.flask.Y,
                x2: this.flask.X+this.flask.size*.15, y2: this.flask.Y-this.flask.size
            })

            // left neck of the beaker
            $('#beaker').drawLine({
                strokeStyle: 'black',
                x1: this.flask.X-this.flask.size*.15, y1: this.flask.Y-this.flask.size,
                x2: this.flask.X-this.flask.size*.15, y2: this.flask.Y-this.flask.size*1.15
            })

            // right neck of the beaker
            $('#beaker').drawLine({
                strokeStyle: 'black',
                x1: this.flask.X+this.flask.size*.15, y1: this.flask.Y-this.flask.size,
                x2: this.flask.X+this.flask.size*.15, y2: this.flask.Y-this.flask.size*1.15
            })

            // the top of the beaker
            $('#beaker').drawEllipse({
                strokeStyle: 'black',
                x: this.flask.X, y: this.flask.Y-this.flask.size*1.15,
                width: this.flask.size*.15*2, height: this.flask.size*.05
            });

            // the lines on the beaker
            $('#beaker').drawArc({
                strokeStyle: 'black',
                x: this.flask.X, y: this.flask.Y-this.flask.size*.7,
                radius: this.flask.size*.5,
                // start and end angles in degrees
                start: 170, end: -170
            });

            $('#beaker').drawArc({
                strokeStyle: 'black',
                x: this.flask.X, y: this.flask.Y-this.flask.size*.9,
                radius: this.flask.size*.5,
                // start and end angles in degrees
                start: 170, end: -170
            });

            $('#beaker').drawArc({
                strokeStyle: 'black',
                x: this.flask.X, y: this.flask.Y-this.flask.size*1.2,
                radius: this.flask.size*.5,
                // start and end angles in degrees
                start: 170, end: -170
            });

            // liquid inside beaker

            // surface of liquid
            $('#beaker').drawEllipse({
                strokeStyle: '#4db8ff',
                fillStyle: 'rgba(179, 224, 255, 0.7)',
                x: this.flask.X,
                y: this.flask.Y-this.flask.size,
                width: this.flask.size*.15*2,
                height: this.flask.size*.05
            });

            $('#beaker').drawLine({
                strokeWidth: 0,
                fillStyle: 'rgba(179, 224, 255, 0.7)',
                x1: this.flask.X-this.flask.size*.15,
                y1: this.flask.Y-this.flask.size,
                x2: this.flask.X+this.flask.size*.15,
                y2: this.flask.Y-this.flask.size,
                x3: this.flask.X+this.flask.size/2,
                y3: this.flask.Y,
                x4: this.flask.X-this.flask.size/2,
                y4: this.flask.Y
            });

            // make the balloon!
          
            // the balloon's neck
            $('#beaker').drawEllipse({
                fillStyle: '#c33',
                x: this.flask.X,
                y: this.flask.Y-this.flask.size*1.12,
                width: this.flask.size*.325,
                height: this.flask.size*.18
            });

            // the balloons 
            $('#beaker').drawRect({
                fillStyle: '#c33',
                x: this.flask.X,
                y: this.flask.Y-this.flask.size*1.2,
                width: 20,
                height: 30
            });
         
            // the balloon's bulb
            $('#beaker').drawEllipse({ // the balloon's bulb
                fillStyle: '#c33',
                x: this.flask.X, y: this.flask.Y-this.flask.size*1.75 - this.gas/2,
                width: this.flask.size*.4+this.gas, height: this.flask.size+this.gas
            });

            // i <= this.pieces/2 ? this.flask.X - ((znRadius*this.flask.size*.015*i)) : this.flask.X + (znRadius*this.flask.size*.015*(this.pieces%i))

            // zinc particles
            let znRadius = this.flask.size*2/(this.flask.size*2/Math.sqrt(((Math.PI*(this.flask.size*2)^2)/this.pieces/Math.PI)));
            let firstX = this.flask.X - znRadius*this.flask.size*.015*(this.pieces/2) + znRadius
            for (i=0; i < this.pieces; i++) {
                $('#beaker').drawArc({
                    fillStyle: '#b3b3b3',
                    x: firstX + znRadius*this.flask.size*.015*i, y: this.flask.Y - this.flask.size*.1,
                    radius: znRadius
                });
            }
            
        }
    },
    mounted: function() {
        this.drawFlask();
        this.$watch(vm => [vm.temp, vm.pieces, vm.molarity, vm.gas], val => {
            this.drawFlask();
        }, {
            immediate: true,
            deep: true
        });

        window.setInterval(() => {
            this.updateGas();
        }, 500);
    }
})